﻿import java.util.Scanner;

public class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
   double ticketEinzelpreis;
       double zuZahlenderGesamtbetrag = fahrkartenbestellungErfassen();
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderGesamtbetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(zuZahlenderGesamtbetrag, eingezahlterGesamtbetrag);
    }
   
       

       


      
   public static double fahrkartenbestellungErfassen() {
	   System.out.print("Ticket Einzelpreis (EURO): "
	       		+ "");
	   Scanner tastatur = new Scanner(System.in);
	       double ticketEinzelpreis = tastatur.nextDouble();
	       
	       //Ticketanzahl
	       //------------
	       System.out.print("Wie viele Tickets wollen Sie mit diesem Betrag kaufen?: ");
	      int ticketAnzahl = tastatur.nextInt();
	       
	       //Berechnung des Betrages
	       //-----------------------
	      double zuZahlenderGesamtbetrag = ticketEinzelpreis * ticketAnzahl; 
	      
	      return zuZahlenderGesamtbetrag;
   }
   public static double fahrkartenBezahlen(double zuZahlenderGesamtbetrag) {
	    // Geldeinwurf
        // -----------
	   Scanner tastatur = new Scanner(System.in);
       double eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderGesamtbetrag)
       {
    	   System.out.printf("%s %.2f %s %n","Noch zu zahlen: ",zuZahlenderGesamtbetrag - eingezahlterGesamtbetrag, "EURO");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	  double eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
	   
	   return eingezahlterGesamtbetrag;
   }
   public static void fahrkartenAusgeben() {

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 15; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
   }
   public static void rueckgeldAusgeben(double zuZahlenderGesamtbetrag, double eingezahlterGesamtbetrag) {
	   double rückgabebetrag;
	   // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       rückgabebetrag = (zuZahlenderGesamtbetrag - eingezahlterGesamtbetrag)*-1;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("%s %.2f %s %n","Der Rückgabebetrag in Höhe von ",rückgabebetrag,"EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
   }