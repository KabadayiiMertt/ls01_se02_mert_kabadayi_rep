import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?");
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		double preis = liesDouble("Geben Sie den Nettopreis ein:");

		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		
	
		double berechneGesamtnettopreis = anzahl * preis;
		double berechneGesamtBruttopreis = berechneGesamtnettopreis * (1 + mwst / 100);

		rechnugausgeben(artikel, anzahl,berechneGesamtnettopreis,berechneGesamtBruttopreis, mwst);
		
	}
		
		public static String liesString(String text) {
			System.out.println(text);
			Scanner myScanner = new Scanner(System.in);
			
			String str = myScanner.next();
			
			return str;
		}
		public static int liesInt(String text) {
			System.out.println(text);
			Scanner myScanner = new Scanner(System.in);
			
			int erg = myScanner.nextInt();
			return erg;
			
		}
		
		public static double liesDouble(String text){
			System.out.println(text);
			Scanner myScanner = new Scanner(System.in);
			
			double erg = myScanner.nextDouble();
			
			return erg;
		}
		
		public static double berechneGesamtnettopreis(int anzahl, double preis){
			
			double erg = anzahl * preis;
			
			return erg;
		}
		
		public static double berechneGesamtBruttopreis(double nettogesamtpreis, double mwst){
			
			double erg = nettogesamtpreis * (1 + mwst / 100);
			
			return erg;
		}
		
		public static void rechnugausgeben(String artikel, int anzahl, double
				nettogesamtpreis, double bruttogesamtpreis,
				double mwst) {
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		}

	
	}


